/*
 * TextMagnifier
 * Copyright (c) 2009 nightc http://blog.nightc.com/
 * Date: 2014-11-06
 * 
 */
(function($){
	$.fn.TextMagnifier = function(options){
		var defaults = {
            dir : "top"
        }
        var self = $(this)
        var options = $.extend(defaults, options);
        (function(op) {
        	op.on('input',function() {
        		var str = op.val()
        		var ntop = op.offset().top
        		var nleft = op.offset().left
        		if($('.TextMagnifier').exist()){
        			var fdiv = $('.TextMagnifier')
        		} else {
        			var fdiv = $("<div class=\"TextMagnifier\"></div>")
        			$("body").append(fdiv)
        		}
        		fdiv.css({position:"absolute", 'height':36, 'top':ntop-40, left:nleft,padding:"2px 5px", 'line-height':'32px','font-size':'30px',border:"1px solid #FF6B79",color:"#FF6B79", 'background-color':"#FFFAE5"})
        		fdiv.html(str+" "+str.length+"位")
        	})
        	op.blur(function() {
        		$('.TextMagnifier').remove()
        	})
        })(self)
	}
	$.fn.exist = function(){ 
		if($(this).length >= 1) {
			return true
		}
		return false
	}
})(jQuery);